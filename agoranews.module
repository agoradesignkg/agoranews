<?php

/**
 * @file
 * Contains agoranews.module.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;

/**
 * Implements hook_help().
 */
function agoranews_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the agoranews module.
    case 'help.page.agoranews':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('News module') . '</p>';
      return $output;

    default:
  }
}

/**
 * Gets the node ID of the news overview page.
 *
 * @return int|null
 *   The node ID of the news overview page, or NULL if not set.
 */
function agoranews_get_news_overview_nid(): ?int {
  $overview_nid = \Drupal::config('agoranews.settings')->get('overview_nid');
  if (is_numeric($overview_nid)) {
    $overview_nid = (int) $overview_nid;
  }
  return $overview_nid;
}

/**
 * Sets the node ID of the news overview page.
 *
 * @param int $nid
 *   The node ID of the news overview page.
 */
function agoranews_set_news_overview_nid(int $nid) {
  /** @var \Drupal\Core\Config\Config $config */
  $config = \Drupal::service('config.factory')->getEditable('agoranews.settings');
  $config->set('overview_nid', $nid)->save();
}

/**
 * Returns the URL to news overview page.
 *
 * @return \Drupal\Core\Url|null
 *   The URL to news overview page, or NULL if not set.
 */
function agoranews_get_news_overview_url(): ?Url {
  $nid = agoranews_get_news_overview_nid();
  return !empty($nid) ? Url::fromUri('entity:node/' . $nid) : NULL;
}

/**
 * Checks, whether the current page is a news detail page.
 *
 * @return bool
 *   TRUE, if the current page is a news detail page, FALSE otherwise.
 */
function agoranews_current_page_is_news_detail_page(): bool {
  if (\Drupal::routeMatch()->getRouteName() == 'entity.node.canonical' && $node = \Drupal::routeMatch()->getParameter('node')) {
    return $node->bundle() === 'news';
  }
  return FALSE;
}
