<?php

namespace Drupal\agoranews\Plugin\facets\processor;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\facets\FacetInterface;
use Drupal\facets\Processor\BuildProcessorInterface;
use Drupal\facets\Processor\ProcessorPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * The news target url processor alters the link target of facet items.
 *
 * The target is altered, if the current page is not the news overview page.
 *
 * @FacetsProcessor(
 *   id = "news_target_url",
 *   label = @Translation("News target url"),
 *   description = @Translation("Alters the link target of facet items to the news overview page."),
 *   stages = {
 *     "build" = 99,
 *   }
 * )
 */
class NewsTargetUrlProcessor extends ProcessorPluginBase implements ContainerFactoryPluginInterface, BuildProcessorInterface {

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * Constructs a new NewsTargetUrlProcessor object.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(array $configuration, string $plugin_id, $plugin_definition, RequestStack $request_stack) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->currentRequest = $request_stack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build(FacetInterface $facet, array $results) {
    // No results are found for this facet, so don't try to create urls.
    if (empty($results)) {
      return $results;
    }

    // No overview page to link to, so we can early exit.
    $overview_nid = agoranews_get_news_overview_nid();
    if (empty($overview_nid)) {
      return $results;
    }

    /** @var \Drupal\facets\Result\ResultInterface $result */
    foreach ($results as $result) {
      $url = $result->getUrl();
      if ($url && $url->getRouteName() == 'entity.node.canonical') {
        if ($url->getRouteParameters()['node'] != $overview_nid) {
          $url->setRouteParameter('node', $overview_nid);
        }
      }
    }

    return $results;
  }

}
